----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:00:22 05/17/2022 
-- Design Name: 
-- Module Name:    exprecionesLogicas - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity exprecionesLogicas is
    Port ( A,B,C : in  STD_LOGIC;
           F0,F1,F2,F3 : out  STD_LOGIC);
end exprecionesLogicas;

architecture Behavioral of exprecionesLogicas is
begin
F0 <= (A or B) and C;
F1 <= (A and C) or (C and B);
F2<= B and C;
F3<= A or C;
end Behavioral;

