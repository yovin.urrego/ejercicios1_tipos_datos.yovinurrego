----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    08:35:17 05/18/2022 
-- Design Name: 
-- Module Name:    Ejerciocio26 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity Ejerciocio26 is
    Port ( a, b, c : in  STD_LOGIC;
	ab, cb : inout  STD_LOGIC;
           y : out  STD_LOGIC);
end Ejerciocio26;
architecture Behavioral of Ejerciocio26 is
begin
ab <= '0' when a = '0' else
		'1' when (a ='1' and b = '1');  
cb <= '0' when c ='0' else
		'1' when (c ='1' and b = '1'); 	
y <= ab or cb or (a and c);  
end Behavioral;

