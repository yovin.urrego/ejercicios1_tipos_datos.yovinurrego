----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:21:39 05/17/2022 
-- Design Name: 
-- Module Name:    ejercicio117 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ejercicio117 is
    Port ( a,b : in  STD_LOGIC_VECTOR (0 to 3);
           f : out  STD_LOGIC);
end ejercicio117;

architecture Behavioral of ejercicio117 is

begin
f <= (a(0) or b(0)) and (a(0) or b(0)) and(a(0) or b(0)) and(a(0) or b(0));
end Behavioral;

