----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:19:47 05/17/2022 
-- Design Name: 
-- Module Name:    MotorVentilador - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity MotorVentilador is
    Port ( a : in  STD_LOGIC;
           b : in  STD_LOGIC;
           letf : out  STD_LOGIC;
           right : out  STD_LOGIC);
end MotorVentilador;

architecture Behavioral of MotorVentilador is
begin
process (a, b) begin
if (a='1') then
right <= '1'; elsif (b ='1') then letf <= '1'; else right <= '0';
 letf <= '0';
end if;
end process;
end Behavioral;

