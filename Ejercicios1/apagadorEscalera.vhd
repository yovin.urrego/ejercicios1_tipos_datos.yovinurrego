----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:15:19 05/17/2022 
-- Design Name: 
-- Module Name:    apagadorEscalera - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity apagadorEscalera is
    Port ( a, b : in  STD_LOGIC;
           c : out  STD_LOGIC);
end apagadorEscalera;

architecture Behavioral of apagadorEscalera is
begin
c <= (not b) or (a and (not b));
end Behavioral;

