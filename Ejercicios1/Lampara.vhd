----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:10:32 05/17/2022 
-- Design Name: 
-- Module Name:    Lampara - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Lampara is
    Port ( boton : in  STD_LOGIC;
           luz : out  STD_LOGIC);
end Lampara;

architecture Behavioral of Lampara is
begin
process (boton) begin 
if boton = '1' then 
luz <= '1';
else luz <= '0';
end if;
end process;
end Behavioral;

