----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:38:55 05/17/2022 
-- Design Name: 
-- Module Name:    Silos - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity PanelArroz is
    Port ( a , b : in  STD_LOGIC;
           c, d, e : out  STD_LOGIC);
end PanelArroz;

architecture Behavioral of PanelArroz is
begin
c <= '1' when (a or b) = '0' else '0'; 
d <= '1' when (a and b)= '1' else '0';
e <= '1' when (a and b)= '1' else '0';
end Behavioral;

