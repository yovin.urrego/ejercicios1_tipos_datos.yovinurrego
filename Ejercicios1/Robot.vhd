----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:45:15 05/17/2022 
-- Design Name: 
-- Module Name:    Robot - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Robot is
    Port ( sensor : in  STD_LOGIC_VECTOR (0 TO 3);
           robot : out  STD_LOGIC);
end Robot;

architecture Behavioral of Robot is
begin
process (sensor) begin 
if sensor = "1111" then 
robot <= '1'; 
else robot <= '0';
end if;
end process;
end Behavioral;

